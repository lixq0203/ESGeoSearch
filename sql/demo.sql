/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50718
Source Host           : 127.0.0.1:3306
Source Database       : essearchdemo

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2021-02-08 14:51:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for housedemo
-- ----------------------------
DROP TABLE IF EXISTS `housedemo`;
CREATE TABLE `housedemo` (
  `id` varchar(255) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `addTime` datetime DEFAULT NULL,
  `cityId` int(11) DEFAULT NULL,
  `cityName` varchar(255) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of housedemo
-- ----------------------------
INSERT INTO `housedemo` VALUES ('1212121', '测试title', '测试地址', '2021-02-08 13:54:31', '2', '北京', null, null, '13.30');
