# EsGeoSearch

#### 介绍

EsGeoSearch 是一个采用spring-data-elasticsearch 操作Elasticsearch的程序，
### 近期更新（1.0.0发布）
- 初始版本支持创建索引和分页查询

### 计划增加功能
-  1、模糊查询。
-  2、限定范围查询。
-  3、聚类。
-  4、5、6、7、、、、、。


#### 使用说明
-  1、初始化。
-  http://localhost:8181/api/es/init
-  2、分页查询。
-   最后的参数是页码从0开始，一页20条
-  http://localhost:8181/api/es/search/0

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
