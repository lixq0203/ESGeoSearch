package com.essearch.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.essearch.entity.HouseItem;
import com.essearch.es.HouseRepository;
import com.essearch.service.EsHouseService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/es")
public class EsHouseSearchController {
	@Autowired
	EsHouseService esHouseSearch;

	@Autowired
	HouseRepository houseRepository;

	//初始化索引
	@GetMapping("/init")
	public void creatIndex(HttpServletRequest request) {

		esHouseSearch.deleteAll();
		esHouseSearch.createIndex();
		List<HouseItem> list = esHouseSearch.getAllHouse();
		esHouseSearch.saveBatch(list);

	}
	
	//查询某一页的所有数据
	@GetMapping("/search/{page}")
	public List<HouseItem> newHouseSearchByKeywords(@PathVariable("page") int page, HttpServletRequest request) {
		String keywords = request.getParameter("keywords");
		String cityIds = request.getParameter("cityIds");
		log.info("=====newHouseSearchByKeywords======" + keywords);
		int size = 20;
		List<HouseItem> list = esHouseSearch.queryHouse(keywords, cityIds, page, size);
		return list;
	}

	//根据cityid查询所有的数据
	@GetMapping("/search/city{cityId}")
	public List<HouseItem> findByCityId(@PathVariable("cityId") int cityId, HttpServletRequest request) {
		List<HouseItem> list = esHouseSearch.findByCityId(cityId);
		return list;
	}

	/**
	 * and 方式查询 类似sql的like
	 * @param request
	 * @return
	 */
	@GetMapping("/findByCityNameAndAddress")
	public List<HouseItem> findByCityNameAndAddress(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");
		String address = request.getParameter("address");
		return houseRepository.findByCityNameAndAddress(cityName, address);

	}

	/**
	 * 或 like 查询 类似sql的 where a like '' and b lile ''
	 * @param request
	 * @return
	 */
	@GetMapping("/findDistinctHouseItemByCityNameOrAddress")
	public List<HouseItem> findDistinctHouseItemByCityNameOrAddress(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");
		String address = request.getParameter("address");

		return houseRepository.findDistinctHouseItemByCityNameOrAddress(cityName, address);

	}

	/**
	 *   查询非重复的记录
	 * @param request
	 * @return
	 */
	@GetMapping("/findHouseItemDistinctByCityNameOrAddress")
	public List<HouseItem> findHouseItemDistinctByCityNameOrAddress(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");
		String address = request.getParameter("address");

		return houseRepository.findHouseItemDistinctByCityNameOrAddress(cityName, address);

	}

	/**
	 * 忽略大小写示例
	 * @param request
	 * @return
	 */
	@GetMapping("/findByCityNameIgnoreCase")
	public List<HouseItem> findByCityNameIgnoreCase(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");

		return houseRepository.findByCityNameIgnoreCase(cityName);

	}

	/**
	 * 所有条件忽略大小写
	 **/
	@GetMapping("/findByCityNameAndrAddressAllIgnoreCase")
	public List<HouseItem> findByCityNameAndrAddressAllIgnoreCase(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");
		String address = request.getParameter("address");

		return houseRepository.findByCityNameAndrAddressAllIgnoreCase(cityName, address);

	}

	/**
	 * 升序排序示例
	 * @param request
	 * @return
	 */
	@GetMapping("/findByCityNameOrderByCityIdAsc")
	public List<HouseItem> findByCityNameOrderByCityIdAsc(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");

		return houseRepository.findByCityNameOrderByCityIdAsc(cityName);

	}

	/**
	 * 降序排序示例
	 * @param request
	 * @return
	 */
	@GetMapping("/findByCityNameOrderByCityIdDesc")
	public List<HouseItem> findByCityNameOrderByCityIdDesc(HttpServletRequest request) {
		String cityName = request.getParameter("cityName");
		return houseRepository.findByCityNameOrderByCityIdDesc(cityName);

	}

}
