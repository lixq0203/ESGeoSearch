package com.essearch.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(indexName = "es_house", type = "eshouse", indexStoreType = "fs", shards = 5, replicas = 1, refreshInterval = "-1")
public class HouseItem  extends BaseEntity{
	@Id
	@TableId(value = "id", type = IdType.AUTO)
	private String id;
	
	@Field(type = FieldType.Text)
	private String title;
	
	@Field(type = FieldType.Text)
	private String address;

	private int cityId;

	@Field(type = FieldType.Text)
	private String cityName;

	private String latitude;

	private String longitude;

	private Date addTime;
	
	private  BigDecimal price;
}
