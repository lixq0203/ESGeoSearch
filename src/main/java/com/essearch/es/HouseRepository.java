package com.essearch.es;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.essearch.entity.HouseItem;

public interface HouseRepository extends ElasticsearchRepository<HouseItem,String> {
	List<HouseItem> findByCityId(int cityId);
	
	List<HouseItem> findByCityNameAndAddress(String cityName, String address);

	  // Enables the distinct flag for the query
	  List<HouseItem> findDistinctHouseItemByCityNameOrAddress(String cityName, String address);
	  
	  List<HouseItem> findHouseItemDistinctByCityNameOrAddress(String cityName, String address);

	  // Enabling ignoring case for an individual property
	  List<HouseItem> findByCityNameIgnoreCase(String cityName);
	  // Enabling ignoring case for all suitable properties
	  List<HouseItem> findByCityNameAndrAddressAllIgnoreCase(String cityName, String address);

	  // Enabling static ORDER BY for a query
	  List<HouseItem> findByCityNameOrderByCityIdAsc(String cityName);
	  List<HouseItem> findByCityNameOrderByCityIdDesc(String cityName);
}
