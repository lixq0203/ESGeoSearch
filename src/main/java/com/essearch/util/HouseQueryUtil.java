package com.essearch.util;

import java.util.List;
import java.util.Map;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import com.essearch.entity.HouseItem;

public class HouseQueryUtil {
	private static BoolQueryBuilder buildBasicQuery(Map searchMap) {
		// 构建布尔查询
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		
		// 关键字查询
		if (searchMap.get("keywords") != null) {
			System.out.println("=====buildBasicQuery======"+searchMap.get("keywords").toString());
			String keywords = searchMap.get("keywords").toString().toLowerCase();
			boolQueryBuilder.must(
					QueryBuilders.boolQuery().should(QueryBuilders.matchPhrasePrefixQuery("title", keywords))
					.should(QueryBuilders.boolQuery().should(QueryBuilders.matchPhrasePrefixQuery("address", keywords)))
					.should(QueryBuilders.boolQuery().should(QueryBuilders.matchPhrasePrefixQuery("cityName", keywords)))
					);
		}
		if (searchMap.get("cityIds") != null) {
			System.out.println("=====buildBasicQuery======"+searchMap.get("regionIds").toString());
			String[] regions = searchMap.get("cityIds").toString().split("\\,");
			for(String region : regions) {
				boolQueryBuilder.should(QueryBuilders.matchQuery("cityId", region));
			}
			
		}
		System.out.println(boolQueryBuilder);
		return boolQueryBuilder;
	}
	
	public static List<HouseItem> SearchQuery(ElasticsearchOperations elasticsearchOperations, Map searchMap,int page, int size) {
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		Pageable pageable = PageRequest.of(page, size);
		nativeSearchQueryBuilder.withPageable(pageable);// 分页

		  BoolQueryBuilder boolQueryBuilder = buildBasicQuery(searchMap);
		  nativeSearchQueryBuilder.withQuery(boolQueryBuilder);
		  if(searchMap.get("keywords")==null || !searchMap.get("keywords").equals("")) {
		   nativeSearchQueryBuilder.withSort(new FieldSortBuilder("addTime").order(SortOrder.ASC));
		  }
		
		 List<HouseItem> list =  elasticsearchOperations.queryForList(nativeSearchQueryBuilder.build(), HouseItem.class);
		 System.out.println("queryCount:"+list.size());
		 return list;
	}
}
