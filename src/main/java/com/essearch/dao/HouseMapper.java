package com.essearch.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.essearch.entity.HouseItem;

@Mapper
@Repository
public interface HouseMapper extends BaseMapper<HouseItem> {
	List<HouseItem> getAllHouse();
	
}
