package com.essearch.service;

import java.util.Date;
import java.util.List;

import com.essearch.entity.HouseItem;

public interface EsHouseService {

	
public boolean save(HouseItem item);
	
	public void saveBatch(List<HouseItem> items);
	
	public List<HouseItem> queryHouse(String keywords,String cityIds,int page,int size);
		
	public void deleteAll();
	
	public List<HouseItem> findByCityId(int cityId);
	
	public void createIndex();
	
	public List<HouseItem> getAllHouse();
	
}
