package com.essearch.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.essearch.dao.HouseMapper;
import com.essearch.entity.HouseItem;
import com.essearch.es.HouseRepository;
import com.essearch.service.EsHouseService;
import com.essearch.util.HouseQueryUtil;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EsHouseSearchImpl  implements EsHouseService {

	@Autowired 
	ElasticsearchOperations elasticsearchOperations;
	@Autowired 
	HouseRepository houseRepository;
	
	@Autowired
	HouseMapper houseMapper;
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public boolean save(HouseItem item) {
		// TODO Auto-generated method stub
		
		houseRepository.save(item);
		return true;
	}
	@Override
	public void createIndex() {
		if(!elasticsearchOperations.indexExists(HouseItem.class))
		{
			elasticsearchOperations.createIndex(HouseItem.class);
			elasticsearchOperations.putMapping(HouseItem.class);
		}
	}
	
	
	@Override
	public void saveBatch(List<HouseItem> items) {
		
		houseRepository.saveAll(items);
	}

	
	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		elasticsearchOperations.deleteIndex("es_house");
	}

	@Override
	public List<HouseItem> getAllHouse() {
		// TODO Auto-generated method stub
		return houseMapper.getAllHouse();
	}
	@Override
	public List<HouseItem> queryHouse(String keywords, String cityIds, int page, int size) {
		// TODO Auto-generated method stub
		Map searchMap = new HashMap();
		if(keywords!=null &&!keywords.equals("")) {
			   searchMap.put("keywords", keywords);
		}
			   searchMap.put("cityIds", cityIds);
		
		return HouseQueryUtil.SearchQuery(elasticsearchOperations, searchMap, page, size);
	}

	@Override
	public List<HouseItem> findByCityId(int cityId) {
		
		return houseRepository.findByCityId(cityId);
	}

}
